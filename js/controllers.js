/**
 * Snack Box - Caixinha de Contribuição para o Lanche
 * @author Jefferson Araujo | Franco Neto
 *
 */

/**
 * MainCtrl - controller
 */
function MainCtrl() {

    this.userName = 'User';
    this.helloText = 'Welcome in GeeKnow';
    this.descriptionText = 'It is an application skeleton for a typical AngularJS web app. You can use it to quickly bootstrap your angular webapp projects and dev environment for these projects.';

};

/**
 * translateCtrl - Controller for translate
 */
function translateCtrl($translate, $scope) {
    $scope.changeLanguage = function (langKey) {
        $translate.use(langKey);
        $scope.language = langKey;
    };
}

angular
    .module('geeknow')
    .controller('MainCtrl', MainCtrl)
    .controller('translateCtrl', translateCtrl)