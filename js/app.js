/**
 * Snack Box - Caixinha de Contribuição para o Lanche
 * @author Jefferson Araujo | Franco Neto
 *
 */
(function () {
    angular.module('geeknow', [
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                 // Ui Bootstrap
        'pascalprecht.translate'       // Angular Translate
    ])
})();

