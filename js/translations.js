/**
 * Snack Box - Caixinha de Contribuição para o Lanche
 * @author Jefferson Araujo | Franco Neto
 *
 */
function config($translateProvider) {

    $translateProvider
        .translations('en', {

            // Define all menu elements
            DASHBOARD: 'Dashboard',
            APPVIEWS: 'App views',

            // Define some custom text
            WELCOME: 'Welcome GeeKnow'
        })
        .translations('pt', {

            // Define all menu elements
            DASHBOARD: 'Home',
            APPVIEWS: 'App',

            // Define some custom text
            WELCOME: 'Bem vindo ao GeeKnow'
        });

    $translateProvider.preferredLanguage('pt');

}

angular
    .module('geeknow')
    .config(config)
