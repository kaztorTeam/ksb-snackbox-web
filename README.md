# KAZTOR - GeeKnow Snack Box  #

The system can be accessed at: http://snackbox.kaztor.com.br

### Description ###

Snack Box is an application to control payments and spending on snacks for the colaborators of Kaztor.
This project is used for learning purposes in new technologies.

### Project Organization ###

* web
* core

### Tips’n tricks

- Command to download the repository on the server
```
git clone -b <branchName> https://<user>@bitbucket.org/kaztorTeam/ksb-snackbox-web.git
```

- To update the code on the server, enter the project folder and run this command
```bash
git pull
```

- Access to documentation and use of system API
```
<host>:<port>/swagger-ui.html
```

- [How to use Swagger](http://petstore.swagger.io/#/)

- List all Redis data
```bash
redis-cli keys '*'
```

- Run gulp tasks commands on web module to get all web dependencies

```
gulp or gulp build to build an optimized version of your application
gulp serve to launch a browser sync server on your source files
gulp serve:dist to launch a server on your optimized application
gulp test to launch your unit tests with Karma
gulp test:auto to launch your unit tests with Karma in watch mode
gulp protractor to launch your e2e tests with Protractor
gulp protractor:dist to launch your e2e tests with Protractor on the dist files
```

### Repository Organization ###

* **Master** - Branch of backup, contains development code before generating a release. Nomenclature: [master]
* **Development** - Branch that contains the latest development code. Nomenclature: [development]
* **Features** - Branch that contains the code of the various features that are being developed within a Sprint. When you finish all the work in this branch, the generated code must be placed in the branch [development]. At the end of the Sprint, all these branches will be removed. Nomenclature: [features/KSB-001] (where 001 is the user story number or a number/letter random)
* **Versions** - Branch that contains the code of the versions that are in approval and are candidates for production. It is always generated starting from the branch [master]. Nomenclature: [version/0.0.1.RELEASE ]
* **Milestones** - Tag created to freeze the code of a stable release, or a landmark project completed. Nomenclature: [M1 - Milestone 1] in the case of marks or [1.0] in the case of versions

### The seven rules of a great git commit message ###

1. Separate subject from body with a blank line
2. Limit the subject line to 50 characters
3. Capitalize the subject line
4. Do not end the subject line with a period
5. Use the imperative mood in the subject line
6. Wrap the body at 72 characters
7. Use the body to explain what and why vs. how

For example:
```
Summarize changes in around 50 characters or less

More detailed explanatory text, if necessary. Wrap it to about 72
characters or so. In some contexts, the first line is treated as the
subject of the commit and the rest of the text as the body. The
blank line separating the summary from the body is critical (unless
you omit the body entirely); various tools like `log`, `shortlog`
and `rebase` can get confused if you run the two together.

Explain the problem that this commit is solving. Focus on why you
are making this change as opposed to how (the code explains that).
Are there side effects or other unintuitive consequences of this
change? Here's the place to explain them.

Further paragraphs come after blank lines.

 - Bullet points are okay, too

 - Typically a hyphen or asterisk is used for the bullet, preceded
   by a single space, with blank lines in between, but conventions
   vary here

If you use an issue tracker, put references to them at the bottom,
like this:

Resolves: #KSB-3
See also: #KSB-2, #KSB-1
```

### How do I set? ###

* Set your favorite IDE
* Install a client to use git. Recommendation: [SmartGit](http://www.syntevo.com/smartgit)
* Install [NPM](https://www.npmjs.com)
* Install [Bower](http://bower.io)
* Install [Gulp](http://gulpjs.com/)

```
#!python

$ sudo apt-get install nodejs
$ sudo apt-get install npm

```

* Install [Bower](http://bower.io)

```
#!python

$ sudo npm install bower

```

* Install [Gulp](http://gulpjs.com)

```
#!python

$ npm install --save-dev gulp-install

```

### Communication channels ###

* Skype
* E-mail
* Whatsapp